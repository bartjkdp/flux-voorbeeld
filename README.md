# Een voorbeeld met Flux

Deze repository bevat een voorbeeld om [Flux](https://fluxcd.io/) te gebruiken om Kubernetes deployments te automatiseren. Volg [de handleiding in de Haven documentatie](https://haven.commonground.nl/docs/ci-cd) om dit voorbeeld te gebruiken.
